# @hahnpro/flow-cli

## 2.11.0

### Minor Changes

- Update dependencies

## 2.10.0

### Minor Changes

- 3e53fdb: Migrate from tslint to eslint

### Patch Changes

- 8248b70: Replace deprecated querystring lib with native URLSearchParams
