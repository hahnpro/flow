# @hahnpro/hpc-api

## 1.1.0

### Minor Changes

- 85cc9d7: add assetId and assetName to endpoint api and combine parameters to a single object

## 1.0.3

### Patch Changes

- dac742f: changed Mock-API init types to be dependent on real types

## 1.0.2

### Patch Changes

- Improve handling of API_BASE_URL and API_BASE_PATH environment variables

## 1.0.1

### Patch Changes

- 507dd6e: Added tags to Assettype interface
- ab04943: Add timeout for http request queue operations

## 1.0.0

### Major Changes

- 205b556: Split API from main SDK package to its own package.
